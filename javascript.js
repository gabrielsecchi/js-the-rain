function getRandomInterval(min,max) {
  return Math.floor(Math.random()*(max-min+1)+min);
}

function scale(num, in_min, in_max, out_min, out_max) {
  return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

function startRain(dropQuantity) {
  setTimeout(
    function() {
      $(".btn-start-rain").remove();
      for(let i = 0; i < dropQuantity; i++) {
        addDrop();
      }
      
      addLightning();
    },
    100
  );
}

function addDrop(oldId) {

  let topDrop = ( parseInt( getRandomInterval(5, 300) ) * -1 ) + 'px';
  let leftDrop = ( getRandomInterval(0, 100) ) + '%';

  let heightNumber = getRandomInterval(3, 8);
  let heightDrop = heightNumber + 'px';
  let widthDrop = ( scale( heightNumber, 3, 8, 2, 4 ) ) + 'px';

  let timeFall = getRandomInterval(1000, 3000);

  let id = oldId;
  if(oldId === undefined) {
    id = (new Date()).getTime();
    $("body").append('<div class="drop" id="'+id+'"></div>');
  }

  let drop = $('#'+id);
  $(drop)
    .css("top", topDrop)
    .css("left", leftDrop)
    .css("height", heightDrop)
    .css("width", widthDrop)    
    //.css("z-index", heightNumber)
    .animate({top: '110%'}, timeFall, function() {
      addDrop(id);
    })
}

function addLightning() {
  setTimeout(
    function() {
      $('body').removeClass('lightning');
      $('body').addClass('lightning');
      setTimeout(function(){$('body').removeClass('lightning');}, 3000);
      addLightning();
    },
    getRandomInterval(5000, 30000)
  );
}

startRain(400);